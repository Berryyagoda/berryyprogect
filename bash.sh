#!/bin/bash

START=$(date +%s%N)

let "f=2**17"
c=0
while [ $c -lt $f ]; do
 let "t=2**$c"
 printf "$t \n" >> 1.txt
 c=$(($c+1))
done

END=$(date +%s%N)
DOCHLAND=$((($END-$START)/1000000000))
echo "$DOCHLAND"

